package com.flores.s03d1;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        System.out.println("Loops, Switches and Try-Catches\n");

        System.out.println("for Loops\n");

//        Prints the number from 1 to 5
//        var i = 0; = int i = 0;
//        i = i + 1; = i++
        for (var i = 0; i < 5; i++) {
            System.out.println(i+1);
        }

        System.out.println();

//        Loop through the items of an array
        int [] hundreds = {100, 200, 300, 400, 500};
        for (var i = 0; i < hundreds.length; i++){
            System.out.println(hundreds[i]);
        }

        System.out.println();

//        Enhanced for loop
        String[] firstNamesList = {"John", "Paul", "George", "Ringo"};
        for (String name: firstNamesList) {
            System.out.println(name);
        }

//        Nested for loops
        String[][] classroom = new String[3][3];
//        First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
//        Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Alex";
        classroom[1][2] = "Lonzo";
//        Third Row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println();

       for (var row = 0; row < classroom.length; row++) {
//            System.out.println(Arrays.asList(classroom[row]));
           for (String name : classroom[row]){
               System.out.println(name);
           }
       }

        System.out.println();

//       while and do..while loops
//        int x = 0;
//        int y = 10;
        var x = 0;
        var y = 10;

        while (x < 10){
            System.out.println(String.format("Loop number: %d", x));
            x = x + 1; // x++;
        }

        System.out.println();

        do {
            System.out.println(String.format("Countdown: %d", y));
            y = y - 1; // y--;
        } while (y > 0);

        System.out.println();

        System.out.println("Exception handling\n");

        System.out.println();

        var a = 1;

        System.out.println("The value of a is: " + a);

        System.out.println();

        Scanner input = new Scanner(System.in);
        var num = 0; //int
        System.out.println("Input a number between 0-9:");
        try {
            num = input.nextInt();
//            check if num is within range (0-9)
        } catch (Exception err) {
            System.out.println("Invalid input!");
        }
        System.out.println("The number you entered is: " + num);
    }
}
